﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpike : Enemy {

    private Transform player;
    private float vely;
    private Vector2 move;

    protected new void Update()
    {
        if (launched)
        {
            if (player.position.y > transform.position.y + 0.1)
            {
                vely = 1;
            }
            else if (player.position.y < transform.position.y - 0.1)
            {
                vely = -1;
            }
            else
            {
                vely = 0;
            }


            if (player.position.x  > transform.position.x +0.1)
            {
                transform.Translate(4 * Time.deltaTime, vely * Time.deltaTime, 0);
            }
            else if (player.position.x  < transform.position.x -0.1)
            {
                transform.Translate(-4 * Time.deltaTime, vely * Time.deltaTime, 0);
            }
            else
            {
                transform.Translate(0, vely * Time.deltaTime, 0);
            } 
        
        }
    }

    private void Awake()
    {
        Debug.Log("Awake enemyspike");
        player = GameObject.Find("PlayerShip").GetComponent<Transform>();
    }
}

