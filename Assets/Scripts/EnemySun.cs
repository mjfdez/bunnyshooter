﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySun : Enemy {

    protected new void Update()
    {
        if (launched)
        {
            transform.Translate(0, moveSpeed.y * Time.deltaTime, 0);
        }
    }
}
