﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCache {
    
        private Enemy[] enemy;
        private int currentEnemy = 0;


        public EnemyCache(GameObject prefabEnemy, Vector3 position, Transform parent, int numEnemy)
        {
            enemy = new Enemy[numEnemy];

            Vector3 tmpPosition = position;
            for(int i = 0; i < numEnemy; i++)
            {
                enemy[i] = GameObject.Instantiate(prefabEnemy, tmpPosition, Quaternion.identity, parent).GetComponent<Enemy>();
                enemy[i].name = prefabEnemy.name + "_enemy_" + i;
                tmpPosition.x += 1;
            }
        }

        public Enemy GetEnemy()
        {
            if(currentEnemy > enemy.Length - 1)
            {
                currentEnemy = 0;
            }

            return enemy[currentEnemy++];
        }
    }